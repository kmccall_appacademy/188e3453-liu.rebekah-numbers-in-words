class Fixnum

  def up_to_thousand
    word_hash = { 0=>"zero",1=>"one",2=>"two",3=>"three",4=>"four",5=>"five",6=>"six",7=>"seven",8=>"eight",9=>"nine", 10=>"ten",11=>"eleven",12=>"twelve",13=>"thirteen",14=>"fourteen",15=>"fifteen", 16=>"sixteen", 17=>"seventeen", 18=>"eighteen",19=>"nineteen",20=>"twenty",30=>"thirty",40=>"forty",50=>"fifty",60=>"sixty",70=>"seventy",80=>"eighty",90=>"ninety"}

    int = self
    result = []
    while int > 0
      if word_hash.keys.include?(int)
        result << word_hash[int]
        int = 0
      elsif int < 100
        result << [word_hash[int - int%10], word_hash[int%10]]
        int = 0
      elsif int < 10**3
        result << [word_hash[int/100],"hundred"]
        int = int - int/100*100
      end
    end
    result.join(" ")
  end

  def remove_ends(num)
    (self - self%(num/10**3) - self/num*num)/(num/10**3)
  end

  def in_words
    return "zero" if self == 0


    result = []
    if self < 10**3
      result << self.up_to_thousand
    elsif self < 10**6
      result << (self.remove_ends(10**6)).up_to_thousand
      result << "thousand"
      result << (self.remove_ends(10**3)).up_to_thousand if self.remove_ends(10**3) != 0
    elsif self < 10**9
      result << (self.remove_ends(10**9)).up_to_thousand
      result << "million"
      result << (self.remove_ends(10**6)).up_to_thousand if self.remove_ends(10**6) != 0
      result << "thousand" if self.remove_ends(10**6) != 0
      result << (self.remove_ends(10**3)).up_to_thousand if self.remove_ends(10**3) != 0
    elsif self < 10**12
      result << (self.remove_ends(10**12)).up_to_thousand
      result << "billion"
      result << (self.remove_ends(10**9)).up_to_thousand if self.remove_ends(10**9) != 0
      result << "million" if self.remove_ends(10**9) != 0
      result << (self.remove_ends(10**6)).up_to_thousand if self.remove_ends(10**6) != 0
      result << "thousand" if self - self/10**6*10**6 - self%10**3 != 0
      result << (self.remove_ends(10**3)).up_to_thousand if self.remove_ends(10**3) != 0
    elsif self < 10**15
      result << (self.remove_ends(10**15)).up_to_thousand
      result << "trillion"
      result << (self.remove_ends(10**12)).up_to_thousand if self.remove_ends(10**12) != 0
      result << "billion" if self.remove_ends(10**12) != 0
      result << (self.remove_ends(10**9)).up_to_thousand if self.remove_ends(10**9) != 0
      result << "million" if self.remove_ends(10**9) != 0
      result << (self.remove_ends(10**6)).up_to_thousand if self.remove_ends(10**6) != 0
      result << "thousand" if self - self/10**6*10**6 - self%10**3 != 0
      result << (self.remove_ends(10**3)).up_to_thousand if self.remove_ends(10**3) != 0
    else
      nil
    end

    result.join(" ")
  end

end

1_888_259_040_036.in_words
